# README #

#tiesinės regresijos modelio sudarymas

Eiga:

1.Patikrinamas kintamųjų ir stebinių kiekis.

wc -l X.txt
wc -l y.txt

2.Terminale apjungiame ir tuomet suskaidome failus į train_yX (70%), validation_yX (20%) ir test_yX imtis (10%).

paste -d ' ' y.txt X.txt > xY.txt
mungy split xY.txt 0.7,0.2,0.1 --filenames train_yX.txt,validation_yX.txt,test_yX.txt

3.Transformuojama į .vw formatus

mungy csv2vw train_yX.txt --delim=' ' --label=0 --no-binary-label --no-header -o train_yX.vw
mungy csv2vw validation_yX.txt --delim=' ' --label=0 --no-binary-label --no-header -o validation_yX.vw
mungy csv2vw test_yX.txt --delim=' ' --label=0 --no-binary-label --no-header -o test_yX.vw 

4.Apmokome modelį, naudodami train.vw (passes 10, quadratic features, l1)

Apmokomi  modeliai su skirtingais l1. Išsaugomi readable model, serial model, bei pastarųjų keliai.
Apskaičiuojame visų apmokytų modelių prognozes bei jas išsaugome. Taip pat išsaugome inverted hash failą.
Įvertinamos visų modelių metrikos MSE, MAE ir R2 pagal turimas prognozes ant validavimo imties.

python regression.py

5.Apjungiamos apmokymo ir validavimo imtys:

cat train_yX.vw validation_yX.vw > joined.vw

6.Apmokomas geriausias modelis assignment1: l1 = 0.0001, assignment2: l1 = 0.0001.
 
7.Užrašoma modelio lygtis.

cat hash.invert_hash

