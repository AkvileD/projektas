def mk_dir_model(L1=None, L2=None, dir_output=None):
    part_L1 = "__L1_{}".format(L1) if L1 else ""
    part_L2 = "__L2_{}".format(L2) if L2 else ""
    fname_model = "model{}{}".format(part_L1, part_L2)
    return os.path.join(dir_output, fname_model) if dir_output else fname_model
def mk_cmd_vw_train(path_model, path_readable_model, path_data=None, L1=None, L2=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--normalized",
        "--cache",
        "--holdout_off",
        "--quadratic nn",
        "--loss_function squared",
        "--passes 10",
        "--data {}".format(path_data) if path_data and path_data != "-" else "",
        "--final_regressor {}".format(path_model),
        "--readable_model {}".format(path_readable_model),
        "--l1 {}".format(L1) if L1 else "",
        "--l2 {}".format(L2) if L2 else "",
    ])
    return cmd
	
def mk_cmd_vw_predict(path_invert_hash, path_initial_regressor, path_predictions, path_data=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--invert_hash {}".format(path_invert_hash),
        "--testonly",
        "--data {}".format(path_data) if path_data and path_data != "-" else "", 
        "--initial_regressor {}".format(path_initial_regressor),
        "--predictions {}".format(path_predictions),
    ])
    return cmd

def mk_cmd_vw_pasted(path_predictions,path_data, path_combined):
    cmd = " ".join([
        "! paste -d",
        "' '",
        "{}".format(path_predictions) if path_predictions else "" ,
        "{}".format(path_data) if path_data else "" ,
        ">",
        "{}".format(path_combined) if path_combined else ""
    ])
    return cmd
    
def mk_cmd_vw_metrics(path_combined, path_metrics):
    cmd = " ".join([
        "! mpipe metrics-reg",
        "--has-r2",
        "{}".format(path_combined) if path_combined else "" ,
        "-o",
        "{}".format(path_metrics) if path_metrics else ""
    ])
    return cmd

import os
import subprocess
dir_output_base = "/home/vagrant/synced_dir/data/assignment_2"
os.makedirs(dir_output_base, exist_ok=True)

dir_models = os.path.join(dir_output_base, "modelsL2")
#L1s = [0.0001]
#L1s = [0.1, 0.01, 0.001, 0.0001, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]
L2s = [0.1, 0.01, 0.001, 0.0001, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]
path_training = "/home/vagrant/synced_dir/data/assignment_2/train_yX.vw"
path_validation = "/home/vagrant/synced_dir/data/assignment_2/validation_yX.vw"
FILENAME_METRICS = "metrics.json"
FILENAME_VW_REGRESSOR = "regressor.vw_model"
FILENAME_READABLE_MODEL = "readable_model.txt"
FILENAME_PREDICTIONS = "y_predicted.txt"
FILENAME_Inverted_hash = "hash.invert_hash"
FILENAME_combined = "combined.txt"
for L2 in L2s:
#for L1 in L1s:

	dir_model = mk_dir_model(L2, dir_output=dir_models)
	os.makedirs(dir_model,exist_ok=True)
	cmd_train = mk_cmd_vw_train(path_model=os.path.join(dir_models, dir_model, FILENAME_VW_REGRESSOR), path_readable_model=os.path.join(dir_models, dir_model,FILENAME_READABLE_MODEL),path_data=path_training,L1=None,L2=L2)
	subprocess.run(cmd_train, shell=True)

    
     #TODO: predict on validation set
	cmd_predict = mk_cmd_vw_predict(path_initial_regressor=os.path.join(dir_models, dir_model, FILENAME_VW_REGRESSOR), path_predictions=os.path.join(dir_models,dir_model, FILENAME_PREDICTIONS),path_invert_hash=os.path.join(dir_models, dir_model, FILENAME_Inverted_hash),path_data=path_validation)
	subprocess.run(cmd_predict, shell = True)
	print(f"done training model")
    
     #TODO: compute regression metrics
	cmd_paste =mk_cmd_vw_pasted(path_predictions=os.path.join(dir_models, dir_model, FILENAME_PREDICTIONS),path_data= path_validation, path_combined=os.path.join(dir_models, dir_model, FILENAME_combined))
	subprocess.run(cmd_paste, shell = True)
	cmd_metrics = mk_cmd_vw_metrics(path_combined =os.path.join(dir_models, dir_model, FILENAME_combined),path_metrics =os.path.join(dir_models, dir_model, FILENAME_METRICS))
	subprocess.run(cmd_metrics, shell = True)
